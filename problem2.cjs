// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is? Log the make and model into the console in the format of: "Last car is a *car make goes here* *car model goes here*" //
function problem2(inventory){
    if(inventory==null || inventory.length==0){
        return []
    }
    if(Array.isArray(inventory)){
        var year = inventory[0].car_year
        var ind = 0
        for(var a = 0;a<inventory.length;a++){
            if(inventory[a].car_year > year){
                ind = a
            }
        }
        return [inventory[ind]]
    }
    return []
}
module.exports = problem2;


