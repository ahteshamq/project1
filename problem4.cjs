// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned. //
function problem4(inventory){
    let arr = [];
    if(inventory==null || inventory.length==0){
        return []
    }
    if(Array.isArray(inventory)){
        for(var i = 0;i<inventory.length;i++){
            arr.push(inventory[i].car_year);
        }
        
        return arr
    }
    return []
}

module.exports = problem4;