//console.log('Hello World');
//==== Problem #1 ==== // The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: "Car 33 is a *car year goes here* *car make goes here* *car model goes here*" //
const problem1 = (inventory, ID) => {
    if(inventory==null || inventory.length==0 || ID==null || !(Number.isInteger(ID))){
        return []
    }
    if(Array.isArray(inventory)){
        for(var a = 0;a<inventory.length;a++){
            if (inventory[a].id == ID){
                console.log(`Car 33 is a ${inventory[a].car_year} ${inventory[a].car_make} ${inventory[a].car_model}`)
                ans = [inventory[a]]
                return ans
            }
        }
    }
    else{
        return []
    }
    
}

module.exports = problem1;