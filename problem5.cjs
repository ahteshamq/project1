// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length. //

const car = require("./problem4.cjs")
function problem5(inventory){
    if(inventory==null || inventory.length==0){
        return []
    }
    if(Array.isArray(inventory)){
        arrayYear = car(inventory);
        arrayOlder = []
        for(var i =0;i<arrayYear.length;i++){
            if(arrayYear[i]<2000){
                arrayOlder.push(arrayYear[i])
            }
        }
    return arrayOlder
    }
    return []
}

module.exports = problem5;

