 // The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
function problem3(inventory){
    let arr = [];
    if(inventory==null || inventory.length==0){
        return []
    }
    if(Array.isArray(inventory)){
        for(var i = 0;i<inventory.length;i++){
            arr.push(inventory[i].car_model);
        }
        arr.sort();
    // for(var i = 0;i<inventory.length;i++){
    //     console.log(arr[i]);
    // }
        return arr
    }
    return []
}

module.exports = problem3;
